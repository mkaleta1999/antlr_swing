
tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : 
        (e+=blok | e+=expr | d+=decl)* 
        -> main(instr={$e},deklaracje={$d})
        ;

blok:
     ^(LCB {enterScope();} (e+=blok | e+=expr | d+=decl)* {leaveScope();} ) 
     -> scopeTmpl(instr={$e},deklaracje={$d})
     ;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> declare(name={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> substract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> writeVar(name={$i1.text}, value={$e2.st})
        | ID -> readVar(name={$ID.text})
        | INT  {numer++;}                     -> int(i={$INT.text},j={numer.toString()})
    ; 
     