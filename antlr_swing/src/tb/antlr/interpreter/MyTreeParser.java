package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	private GlobalSymbols memory = new GlobalSymbols();
	private LocalSymbols locals = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void newVar(String text) {
		memory.newSymbol(text);
	}
	
	protected void setVar(String name, Integer value) {
		memory.setSymbol(name, value);
	}
	
	protected Integer getVar(String name) {
		return memory.getSymbol(name);
	}
}
