tree grammar TExpr1;

options {
  tokenVocab   = Expr;
  ASTLabelType = CommonTree;
  superClass   = MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog
  :
  (
    expr
    | printExpr
    | variableDeclaration
  )*
  ;

printExpr
  :
  PRINT expr 
            {
             drukuj ($expr.text + " = " + $expr.out.toString());
            }
  ;

variableDeclaration
  :
  ^(VAR ID)
  
  {
   newVar($ID.text);
  }
  ;


expr returns [Integer out]
  :
  ^(PLUS e1=expr e2=expr)
  {
   $out = $e1.out + $e2.out;
  }
  |
  ^(MINUS e1=expr e2=expr)
  {
   $out = $e1.out - $e2.out;
  }
  |
  ^(MUL e1=expr e2=expr)
  {
   $out = $e1.out * $e2.out;
  }
  |
  ^(DIV e1=expr e2=expr)
  {
   $out = $e1.out / $e2.out;
  }
  |
  ^(EXP e1=expr e2=expr)
  {
   $out=(int)Math.round(Math.pow($e1.out,$e2.out));
  }
  |
  ^(LSHIFT e1=expr e2=expr)
  {
   $out=$e1.out << $e2.out;
  }
  |
  ^(RSHIFT e1=expr e2=expr)
  {
   $out=$e1.out >> $e2.out;
  }
  |
  ^(PODST ID e1=expr)
  {
   setVar($ID.text,$e1.out);
   $out=getVar($ID.text);
  }
  | 
  ID 
  {
   $out = getVar($ID.text);
  }
  | 
  INT 
  {
   $out = getInt($INT.text);
  }
  ;
