grammar Expr;

options {
  output       = AST;
  ASTLabelType = CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
  :
  (
    stat
    | group
  )+
  EOF!
  ;

group
  :
  LCB^
  (
    stat
    | group
  )*
  RCB!
  ;

stat
  :
  expr NL
    -> expr
  | PRINT expr NL
    -> PRINT expr
  | VAR ID PODST expr NL
    ->
      ^(VAR ID)
      ^(PODST ID expr)
  | VAR ID NL
    ->
      ^(VAR ID)
  | ID PODST expr NL
    ->
      ^(PODST ID expr)
  | function NL
    ->
      ^(FUN function)
  | NL
    ->
  ;

function
  :
  TYP ID LP params? RP 
  ;

params
  :
  TYP ID (COMMA TYP ID)*
  ;

expr
  :
  multExpr
  (
    PLUS^ multExpr
    | MINUS^ multExpr
    | LSHIFT^ multExpr
    | RSHIFT^ multExpr
  )*
  ;

multExpr
  :
  atom
  (
    MUL^ atom
    | DIV^ atom
    | EXP^ atom
  )*
  ;

atom
  :
  INT
  | ID
  | LP! expr RP!
  ;

FUN: ;

VAR
  :
  'var'
  ;

TYP
  :
  'int'
  | 'float'
  ;

PRINT
  :
  'print'
  ;

ID
  :
  (
    'a'..'z'
    | 'A'..'Z'
    | '_'
  )
  (
    'a'..'z'
    | 'A'..'Z'
    | '0'..'9'
    | '_'
  )*
  ;

INT
  :
  '0'..'9'+
  ;

COMMA
  :
  ','
  ;

NL
  :
  ';'
  ;

WS
  :
  (
    ' '
    | '\t'
    | '\r'
    | '\n'
  )+
  
  {
   $channel = HIDDEN;
  }
  ;

LP
  :
  '('
  ;

RP
  :
  ')'
  ;

LCB
  :
  '{'
  ;

RCB
  :
  '}'
  ;

PODST
  :
  '='
  ;

PLUS
  :
  '+'
  ;

MINUS
  :
  '-'
  ;

MUL
  :
  '*'
  ;

DIV
  :
  '/'
  ;

EXP
  :
  '^'
  ;

LSHIFT
  :
  '<<'
  ;

RSHIFT
  :
  '>>'
  ;
